# Install R version 3.5
FROM rocker/shiny-verse:3.5.2

# Install R packages that are required n.b.
# RUN R -e "install.packages('magick', dependencies = TRUE, repos='http://cran.rstudio.com/')"

RUN mkdir /root/example_app
COPY example_app /root/example_app

# Make the app available at port 3838
EXPOSE 3838

# run the app
CMD ["R", "-e", "shiny::runApp('/root/example_app', host='0.0.0.0', port=3838)"]
