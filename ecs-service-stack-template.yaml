AWSTemplateFormatVersion: 2010-09-09
# these Parameters must be specificed when the template is used for stack creation
Parameters:
# this will be appended to artesia.io, so the url will be something like
# newapp.root.io
#  SubDomain:
#    Type: String
#    Description: >-
#        This will be appended to the root domain,
#        so the url will be something like
#        newapp.root.io
# this is how much cpu the app needs (256 = 1 core I think)
  CPU:
     Type: String
     Default: "256"
     Description: Enter how much cpu the app needs (256 = 1 core I think)
# this is how much memory the app needs to run
  Memory:
     Type: String
     Default: "1024"
     Description: Enter how much memory the app needs to run
# this must be a number not currently used by the existing rules
# on port 443 listener on artesia-apps alb
  RulePriority:
     Type: Number
     Default: 100
     Description: >-
         Must be a number not currently used by the existing rules
         on port 443 listener on the alb
# for internal use only, but probably makes sense to match with SubDomain
  AppName:
     Type: String
     Default: example-app
     Description: >-
         For internal use only, but probably makes sense to match with SubDomain
         N.B. this name is shared across service, targetgroup, TaskDefinition
         and logs
# this must be an existing ECRRepository
  ECRRepository:
    Type: String
    Default: example_app
    Description: >-
        This must be an existing ECRRepository
# the image tag you want to use for the app, in most cases will just the default
# latest
  RepositoryTag:
    Type: String
    Default: latest
    Description: >-
        Image tag you want to use for the app
        in most cases will just the default
# then the resources of the stack (which use the Parameters)
Resources:
#  Repo:
#    Type: AWS::ECR::Repository
#    Properties:
#      RepositoryName: example_app
# the service which runs the task
  Service:
    Type: AWS::ECS::Service
    Properties:
      ServiceName: !Ref AppName
      Cluster: toms-cluster
      LoadBalancers:
        - TargetGroupArn: !Ref TargetGroup
          ContainerName: !Ref AppName
          ContainerPort: 3838
      DesiredCount: 1
      LaunchType: FARGATE
      PlatformVersion: LATEST
      TaskDefinition: !Ref TaskDefinition
      DeploymentConfiguration:
        MaximumPercent: 200
        MinimumHealthyPercent: 100
      NetworkConfiguration:
        AwsvpcConfiguration:
          Subnets:
            - subnet-071f710e49d105203
            - subnet-06caf11522e132155
          SecurityGroups:
            - sg-0e1c458c7b5e7fe67
          AssignPublicIp: ENABLED
      HealthCheckGracePeriodSeconds: 0
      SchedulingStrategy: REPLICA
    DependsOn:
         - TaskDefinition
         - TargetGroup
         - ListenerRule
         - LogGroup
# the task definition, which hosts the container
  TaskDefinition:
    Type: "AWS::ECS::TaskDefinition"
    Properties:
      ContainerDefinitions:
        - Name: !Ref AppName
          Image: !Join ['', [304313199173.dkr.ecr.eu-west-1.amazonaws.com/, !Ref ECRRepository, ':', latest]]
          MemoryReservation: 500
          PortMappings:
            - ContainerPort: 3838
              HostPort: 3838
              Protocol: tcp
          Essential: true
          LogConfiguration:
            LogDriver: awslogs
            Options:
              awslogs-group: !Join ['/', ['/ecs', !Ref AppName]]
              awslogs-region: eu-west-1
              awslogs-stream-prefix: ecs
      Family: !Ref AppName
      ExecutionRoleArn: arn:aws:iam::304313199173:role/ecs-shared-infrastructure-ECSTaskExecutionRole-1VYQ0EQ74G4FC
      NetworkMode: awsvpc
      RequiresCompatibilities:
        - FARGATE
      Cpu: !Ref CPU
      Memory: !Ref Memory
# the target group (which the load balancer forwards traffic onto)
  TargetGroup:
      Type: AWS::ElasticLoadBalancingV2::TargetGroup
      Properties:
        Name: !Ref AppName
        TargetType: ip
        VpcId: vpc-001cca57649b7f4c3
        Port: 80
        Protocol: HTTP
# the listener which the
  ListenerRule:
    Type: AWS::ElasticLoadBalancingV2::ListenerRule
    Properties:
      ListenerArn: arn:aws:elasticloadbalancing:eu-west-1:304313199173:listener/app/ecs-s-Publi-M60PEMNP705L/f7945ec300b87750/513dc811dc49eb86
      Actions:
        - Type: forward
          TargetGroupArn: !Ref TargetGroup
      Conditions:
        - Field: path-pattern
          Values:
           - /example-app/*
      Priority: !Ref RulePriority
# create a log group for the logs of the container
  LogGroup:
    Type: AWS::Logs::LogGroup
    Properties:
      LogGroupName: !Join ['/', ['/ecs', !Ref AppName]]
